package dam2.pmdm.u2.pmddmm_eval1_beatriz_ruiz_maximo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class PaginaEveris extends AppCompatActivity {

    Toolbar toolbar;
    WebView everis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagina_everis);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        everis = (WebView) findViewById(R.id.webEveris);
        everis.loadUrl("https://www.everisschool.com/");
    }
    //Crea el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menueveris, menu);
        return true;
    }
    //Crea las opciones del menú
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.googlemaps){
            abrirMaps();
        }else if (id == R.id.paginaeveris){
            abrirEveris();
        }else if (id == R.id.informacion){
            mostrarInformacion();
        }
        return true;
    }
    //Abre el google maps si es la opción elegída en el menú
    public void abrirMaps(){
        Uri maps = Uri.parse("geo:41.6269323,-4.717817");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, maps);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }
    //Abre la página de Everis en el navegador del teléfono
    public void abrirEveris(){
        Uri web = Uri.parse("https://www.everisschool.com/");
        Intent intent = new Intent(Intent.ACTION_VIEW, web);
        startActivity(intent);

    }
    //Te lleva directamente a la parte de información de la página de Everis
    public void mostrarInformacion(){
        Uri informacion = Uri.parse("https://www.everisschool.com/contactar/");
        Intent intent = new Intent(Intent.ACTION_VIEW, informacion);
        startActivity(intent);
    }
}
