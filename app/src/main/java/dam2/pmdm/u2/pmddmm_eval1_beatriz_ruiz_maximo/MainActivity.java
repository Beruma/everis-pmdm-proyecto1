package dam2.pmdm.u2.pmddmm_eval1_beatriz_ruiz_maximo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    EditText entradaNombre, entradaApellidos, entradaTelefono, entradaEmail;
    Spinner entradaModalidad;
    Button confirmarReserva;

    private final static String[] modalidad = {"PRESENCIAL", "ONLINE"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        entradaNombre = (EditText) findViewById(R.id.EntradaNombre);
        entradaApellidos = (EditText) findViewById(R.id.EntradaApellidos);
        entradaTelefono = (EditText) findViewById(R.id.EntradaTelefono);
        entradaEmail = (EditText) findViewById(R.id.EntradaEmail);
        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, modalidad);
        entradaModalidad = (Spinner) findViewById(R.id.EntradaModalidad);
        entradaModalidad.setAdapter(adapter);
        confirmarReserva = (Button) findViewById(R.id.ConfirmarReserva);

    }
    //Comprueba que los datos introducidos por el usuario son correctos
    public void ComprobarCampos(View vista){

        if (entradaNombre.getText().toString().isEmpty()){
            Toast.makeText(this, R.string.nombrevacio, Toast.LENGTH_LONG).show();

        }else if (!comprobarLongitudNombre()) {

            Toast.makeText(this, R.string.nombrecorto, Toast.LENGTH_LONG).show();
        }else{
            if (entradaApellidos.getText().toString().isEmpty()) {

                Toast.makeText(this, R.string.apeelidovacio, Toast.LENGTH_LONG).show();
            }else if (!comprobarLongitudApellido()) {

                Toast.makeText(this, R.string.apellidocorto, Toast.LENGTH_LONG).show();
            }else{
                if (entradaTelefono.getText().toString().isEmpty()) {

                    Toast.makeText(this, R.string.telefonovacio, Toast.LENGTH_LONG).show();
                }else if (!comprobarTelefono()) {

                    Toast.makeText(this, R.string.telefonovalido, Toast.LENGTH_LONG).show();
                }else{
                    if (entradaEmail.getText().toString().isEmpty()) {

                        Toast.makeText(this, R.string.emailvacio, Toast.LENGTH_LONG).show();
                    }else if (!comprobarEmail()) {

                        Toast.makeText(this, R.string.erroremail, Toast.LENGTH_LONG).show();

                    }else{

                        ActivarReserva();
                    }
                }
            }
        }
    }
    //Activa el cuadro de dialogo para aceptar o cancelar la reserva
    public void ActivarReserva(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        Reservar reservar = new Reservar();
        reservar.show(fragmentManager, "tagConfirmación");
    }
    //Comprueba que la longitud del nombre introducido es mayor a 2 letras
    public boolean comprobarLongitudNombre (){
        if (entradaNombre.getText().toString().length() < 2){
            return false;
        }else {
            return true;
        }
    }
    //Comprueba que la longitud del apellido o los apellidos es mayor a 2 letras
    public boolean comprobarLongitudApellido(){
        if (entradaApellidos.getText().toString().length() < 2){
            return false;
        }else{
            return true;
        }
    }
    //Comprueba que el teléfono introducido es de 9 dígitos y empieza por 6, 7, 8 o 9
    private boolean comprobarTelefono() {
        String telefono = entradaTelefono.getText().toString();
        Pattern pattern = Pattern.compile("^(?:(?:\\+|00)?34)?[6789]\\d{8}$");
        return pattern.matcher(telefono).matches();
    }
    //Comprueba que ek e-mail introducido contiene la arroba y termina en un e-mail válido como por ejemplo gmail.es o .com
    private boolean comprobarEmail(){
        String email = entradaEmail.getText().toString();
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }
}