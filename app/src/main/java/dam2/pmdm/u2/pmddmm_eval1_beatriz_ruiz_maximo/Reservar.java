package dam2.pmdm.u2.pmddmm_eval1_beatriz_ruiz_maximo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.DialogFragment;

public class Reservar extends DialogFragment {

    //Se crea un cuadro de dialogo con "aceptar" o "cancelar" preguntando al usuario si quiere conforma la reserva
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Confirmar Reserva");
        builder.setMessage("Desea confirmar la reserva?");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Aceptar();
            }

        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        return builder.create();
    }
    //Si acepta la reserva pasa a la segunda activity
    public void Aceptar() {
        Intent intent = new Intent(getActivity(), PaginaEveris.class);
        getActivity().startActivityForResult(intent,0);
    }
}
